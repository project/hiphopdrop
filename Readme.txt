HipHopDrop
HipHopDrop is a distribution built using Hack programming language. Hack is a programming language for HHVM. Hack reconciles the fast development cycle of a dynamically typed language with the discipline provided by static typing, while adding many features commonly found in other modern programming languages.

Hack comes with variety of new features such as
1.Type annotations
2.Collections.
3.Asynchronous programming.
4.Generics

HipHopDrop has modified Drupal code base and brought all the great new features of Hack to Drupal. All the features of Drupal have been built using Hack.

Environment Setup
Now since Hack requires HHVM to be installed on Server , it is very important to read these steps very carefully to run HipHopDrop and build your website using it.

1.Install MySQL Database and Nginx Web Server to get best results.

2.Follow the steps mentioned in Digital Ocean Tutorial to install HHVM with Nginx.

3.Download and install HipHopDrop.

MAINTAINER(S)
=============
2017: Vidhatanad https://www.drupal.org/u/vidhatanand
2017: Gaurav Kapoor https://www.drupal.org/u/gauravkapoor
