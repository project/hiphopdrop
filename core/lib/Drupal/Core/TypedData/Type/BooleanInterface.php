<?hh // strict

namespace Drupal\Core\TypedData\Type;

use Drupal\Core\TypedData\PrimitiveInterface;

/**
 * Interface for boolean data.
 */
interface BooleanInterface extends PrimitiveInterface {

}
