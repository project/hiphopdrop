<?hh // strict

namespace Drupal\config_translation\Exception;

/**
 * Provides an exception for configuration mappers with multiple languages.
 */
class ConfigMapperLanguageException extends \RuntimeException {
}
