<?hh
namespace GuzzleHttp\Exception;

class TooManyRedirectsException extends RequestException {}
