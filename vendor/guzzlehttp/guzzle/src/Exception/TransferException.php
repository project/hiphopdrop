<?hh // strict
namespace GuzzleHttp\Exception;

class TransferException extends \RuntimeException implements GuzzleException {}
